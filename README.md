# LGSVL Object Detection with SSD
## Table of Contents  
- [Introduction](#introduction) 
- [Usage](#usage)
- [Features](#features)
- [Updates](#updates)
- [Future](#future)
- [Notes](#notes)

## Introduction
[![](http://i3.ytimg.com/vi/iPaWcyC8oRY/hqdefault.jpg)](https://www.youtube.com/watch?v=iPaWcyC8oRY&t=8s)

This project was developed as part of a research project of the [Fulda University of Applied](https://www.hs-fulda.de/en/home) of the Department of [Master Applied Computer Science](https://www.hs-fulda.de/en/studies/departments/applied-computer-science/study-programmes/study-programmes/msc-in-applied-computer-science)
in fall 2019.
The research project determines how well the object detection with the **[Single Shot MultiBox Detector (SSD)](https://github.com/tooth2/SSD-Object-Detection)** works on computer-generated objects.
Images are captured over a 3D vehicle in the simulation. The SSD is used to classify and frame multiple objects in an image. 
The [**LGSVL simulator (now SVL simulator)**](https://www.svlsimulator.com/) is used to generate the computer-generated images to which SSD is to be applied.
Different types of the same images are compared. For this purpose, the color, depth and mask images of an image are compared.
After the frames, so-called bounding boxes, have been set around the objects in the respective sensor images with the help of SSD,
it is compared whether the boxes match in all image types. The coordinates of the bounding boxes are stored in a json file.

For a demo see [here](https://www.youtube.com/watch?v=iPaWcyC8oRY&t=8s).

**Info**

This project is deprecated and needs to be upgraded.

## Usage
- First start the `simulator.exe` and click on 'open browser'.
- Sign in or log in the web API of the (LG)SVL simulator.
- Select the environment BorregasAve.
- Select as vehicle the Lincoln 2017 MKZ.
- Select 'Simulations' and run/play 'MySimulator'.
- Run the 'models-master/research/object_detection/SimSSDApp.py' via command shell with `python SimSSDApp.py`.
- Press enter and you see the (LG)SVL simulation driving in a circle and capturing images.

## Features
* LGSVL Simulator
* SSD
* Python
* Object Detection
* Tensorflow
* OpenCV

## Updates
No Updates yet.

## Future
The LGSVL simulator is now deprecated and needs to be upgraded for the new SVL simulator. 

## Notes
* The paper 'SSD: Single Shot MultiBox Detector' can be found [here](SSD: Single Shot MultiBox Detector).
* In order to download the **SSD** see [here](https://github.com/tooth2/SSD-Object-Detection).
* Official [homepage](https://www.svlsimulator.com/) of the SVL simulator.
* The SVL repository can be found [here](https://github.com/lgsvl/simulator).
* Tensorflow Model Garden [repository](https://github.com/tensorflow/models).